package com.lbl.microservice.controller;

import com.lbl.microservice.annotation.LblTransactional;
import com.lbl.microservice.entity.TbOrder;
import com.lbl.microservice.mapper.TbOrderMapper;
import com.lbl.microservice.service.TbOrderService;
import com.lbl.microservice.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/18 14:49
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private TbOrderService tbOrderService;

    @Autowired
    private TbOrderMapper tbOrderMapper;

    @RequestMapping("/placeAnOrder")
    public ResponseResult placeAnOrder(@RequestBody TbOrder tbOrder){
        try {
            tbOrderService.placeAnOrder(tbOrder);
            return ResponseResult.success();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseResult.error();
    }
    @RequestMapping("/selectAll")
    public ResponseResult selectAll(){
        return ResponseResult.success(tbOrderMapper.selectAll());
    }

    @RequestMapping("/insert")
    @LblTransactional
    public ResponseResult insert(){
        TbOrder order = new TbOrder();
        order.setCreateTime(new Date());
        order.setMoney((float) 100);
        order.setUserId(1);
        order.setProductName("电脑");
        return ResponseResult.success(tbOrderMapper.insert(order));
    }


    @RequestMapping("/delete")
    @LblTransactional
    public ResponseResult delete(){
        return ResponseResult.success(tbOrderMapper.deleteByPrimaryKey(34));
    }

    @RequestMapping("/update")
    @LblTransactional
    public ResponseResult update(){
        TbOrder order=new TbOrder();
        order.setId(33);
        order.setMoney(999F);
        return ResponseResult.success(tbOrderMapper.updateByPrimaryKeySelective(order));
    }
}
