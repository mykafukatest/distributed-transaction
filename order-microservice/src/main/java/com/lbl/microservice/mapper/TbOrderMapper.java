package com.lbl.microservice.mapper;

import com.lbl.microservice.entity.TbOrder;
import tk.mybatis.mapper.common.Mapper;

public interface TbOrderMapper extends Mapper<TbOrder> {
}