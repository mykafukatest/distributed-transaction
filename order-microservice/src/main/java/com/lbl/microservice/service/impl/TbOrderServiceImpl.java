package com.lbl.microservice.service.impl;

import com.lbl.microservice.annotation.LblTransactional;
import com.lbl.microservice.entity.TbOrder;
import com.lbl.microservice.mapper.TbOrderMapper;
import com.lbl.microservice.service.TbOrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author  刘祖明
 * @date  2019/12/18 14:44
 */
@Service
public class TbOrderServiceImpl implements TbOrderService {

    @Resource
    private TbOrderMapper tbOrderMapper;

    @Override
    @LblTransactional
    public void placeAnOrder(TbOrder tbOrder) {
        if(true){
            throw new RuntimeException("异常回滚.....");
        }
        tbOrderMapper.insert(tbOrder);
    }
}
