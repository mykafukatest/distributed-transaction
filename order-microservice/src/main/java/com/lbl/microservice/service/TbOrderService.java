package com.lbl.microservice.service;

import com.lbl.microservice.entity.TbOrder;

/**
 * @author  刘祖明
 * @date  2019/12/18 14:44
 */
public interface TbOrderService{
    void placeAnOrder(TbOrder tbOrder);
}
