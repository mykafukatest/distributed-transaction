package com.lbl.microservice.microservice;

import com.lbl.microservice.mapper.TbOrderMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class OrderMicroserviceApplicationTests {

    @Autowired
    private TbOrderMapper tbOrderMapper;

    @Test
    void contextLoads() {
        System.out.println(tbOrderMapper.selectAll());
    }

}
