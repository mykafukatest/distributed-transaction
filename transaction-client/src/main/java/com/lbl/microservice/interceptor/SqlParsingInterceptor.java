package com.lbl.microservice.interceptor;

import cn.hutool.json.JSONUtil;
import com.lbl.microservice.entity.TcLog;
import com.lbl.microservice.mapper.TcLogMapper;
import com.lbl.microservice.utils.SpringUtil;
import com.lbl.microservice.utils.SqlParserUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.delete.Delete;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/1/15 14:53
 */
@Intercepts({
        @Signature(
                type = Executor.class,
                method = "update",
                args = {MappedStatement.class, Object.class}
        )
})
@Slf4j
public class SqlParsingInterceptor implements Interceptor{
    @Autowired
    private ThreadLocal<String> transactionGroupThreadLocal;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        String groupId = transactionGroupThreadLocal.get();
        if(StringUtils.isEmpty(groupId)){
            return invocation.proceed();
        }
        Object proceed = updateField(invocation);
        return proceed;
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target,this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
    /**
     * 获取SQL
     * @param configuration
     * @param boundSql
     * @return
     */
    private String getSql(Configuration configuration, BoundSql boundSql) {
        Object parameterObject = boundSql.getParameterObject();
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        String sql = boundSql.getSql().replaceAll("[\\s]+", " ");
        if (parameterObject == null || parameterMappings.size() == 0) {
            return sql;
        }
        TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
        if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
            sql = sql.replaceFirst("\\?", getParameterValue(parameterObject));
        } else {
            MetaObject metaObject = configuration.newMetaObject(parameterObject);
            for (ParameterMapping parameterMapping : parameterMappings) {
                String propertyName = parameterMapping.getProperty();
                if (metaObject.hasGetter(propertyName)) {
                    Object obj = metaObject.getValue(propertyName);
                    sql = sql.replaceFirst("\\?", getParameterValue(obj));
                } else if (boundSql.hasAdditionalParameter(propertyName)) {
                    Object obj = boundSql.getAdditionalParameter(propertyName);
                    sql = sql.replaceFirst("\\?", getParameterValue(obj));
                }
            }
        }
        return sql;
    }


    private String getParameterValue(Object obj) {
        String value = null;
        if (obj instanceof String) {
            value = "'" + obj.toString() + "'";
        } else if (obj instanceof Date) {
            DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CHINA);
            value = "'" + formatter.format(obj) + "'";
        } else {
            if (obj != null) {
                value = obj.toString();
            } else {
                return "null";
            }
        }
        return value;
    }

    private Object updateField(Invocation invocation) throws IllegalAccessException, InvocationTargetException, JSQLParserException {
        Object result=null;
        MappedStatement mappedStatement = (MappedStatement)invocation.getArgs()[0];
        SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
        Object parameter = invocation.getArgs()[1];
        BoundSql boundSql = mappedStatement.getSqlSource().getBoundSql(parameter);
        String sql = getSql(mappedStatement.getConfiguration(), boundSql);
        String tableName = SqlParserUtil.sqlParserForTableName(sql);
        log.info("sql 语句:{}",sql);
        String groupId= transactionGroupThreadLocal.get();
        switch (sqlCommandType){
            case DELETE:
                String d_select = SqlParserUtil.sqlParserForDelete(sql);
                List<Map<String, Object>> d_maps = jdbcTemplate.queryForList(d_select);
                conversionDate(d_maps);
                result=invocation.proceed();
                log.info("解析 sql 语句:{}",d_select);
                if(Integer.parseInt(String.valueOf(result))>0){
                    jdbcTemplate.update("insert into tc_log(group_id,current_id,type,`before`,after,create_date,table_name) values (?,?,?,?,?,?,?)",
                            new Object[]{groupId,"","DELETE", JSONUtil.toJsonStr(d_maps),"",new Date(),tableName});
                }
                break;
            case INSERT:
                result=invocation.proceed();
                if(Integer.parseInt(String.valueOf(result))>0){
                    BeanWrapper src = new BeanWrapperImpl(parameter);
                    Object id = src.getPropertyValue("id");
                    jdbcTemplate.update("insert into tc_log(group_id,current_id,type,`before`,after,create_date,table_name) values (?,?,?,?,?,?,?)",
                            new Object[]{groupId,"","INSERT","",String.valueOf(id),new Date(),tableName});
                }
                break;
            case UPDATE:
                String u_select = SqlParserUtil.sqlParserForUpdate(sql);
                List<Map<String, Object>> u_before_maps = jdbcTemplate.queryForList(u_select);
                conversionDate(u_before_maps);
                log.info("解析 sql 语句:{}",u_select);
                result=invocation.proceed();
                if(Integer.parseInt(String.valueOf(result))>0){
                    List<Map<String, Object>> u_after_maps = jdbcTemplate.queryForList(u_select);
                    jdbcTemplate.update("insert into tc_log(group_id,current_id,type,`before`,after,create_date,table_name) values (?,?,?,?,?,?,?)",
                            new Object[]{groupId,"","UPDATE", JSONUtil.toJsonStr(u_before_maps),JSONUtil.toJsonStr(u_after_maps),new Date(),tableName});
                }
                break;
            default:

        }
        return result;
    }
    private void conversionDate(List<Map<String, Object>> listMap){
        listMap.stream().peek(e-> {
            Set<Map.Entry<String, Object>> entries = e.entrySet();
            Iterator<Map.Entry<String, Object>> iterator = entries.iterator();
            while (iterator.hasNext()){
                Map.Entry<String, Object> entry = iterator.next();
                if(entry.getValue() instanceof Date){
                    entry.setValue(new SimpleDateFormat("yyyy-MM-DD HH:mm:ss").format(entry.getValue()));
                }
            }
        }).collect(Collectors.toList());

    }

}
