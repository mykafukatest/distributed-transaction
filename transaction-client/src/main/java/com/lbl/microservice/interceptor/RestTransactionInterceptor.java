package com.lbl.microservice.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/18 21:16
 */
public class RestTransactionInterceptor implements ClientHttpRequestInterceptor {
    @Autowired
    private ThreadLocal<String> transactionGroupThreadLocal;

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders headers = request.getHeaders();
        headers.add("TRANSACTION_GROUP",transactionGroupThreadLocal.get());
        return execution.execute(request,body);
    }
}
