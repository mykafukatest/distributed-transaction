package com.lbl.microservice.annotation;

import com.lbl.microservice.aspectJ.TransactionalAspect;
import com.lbl.microservice.factory.TransactionFactory;
import com.lbl.microservice.proxy.TransactionalProxyRegistrar;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.lang.annotation.*;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/18 9:21
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({TransactionalProxyRegistrar.class, TransactionFactory.class, TransactionalAspect.class})
@EnableAspectJAutoProxy
@EnableTransactionManagement
public @interface EnableTransactionClient {
}
