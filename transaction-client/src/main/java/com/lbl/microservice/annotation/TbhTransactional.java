package com.lbl.microservice.annotation;

import java.lang.annotation.*;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/18 9:19
 * 无本地事务用此注解
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface TbhTransactional {
}
