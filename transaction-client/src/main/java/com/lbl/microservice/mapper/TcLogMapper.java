package com.lbl.microservice.mapper;

import com.lbl.microservice.entity.TcLog;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/1/15 16:59
 */
public interface TcLogMapper  extends Mapper<TcLog> {
}
