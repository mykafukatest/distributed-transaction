package com.lbl.microservice.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/1/15 16:52
 */
@Data
@Table(name = "tc_log")
public class TcLog {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 全局事务组id
     */
    @Column(name = "group_id")
    private String groupId;

    /**
     * 当前事务id
     */
    @Column(name = "current_id")
    private String currentId;

    /**
     * 方法类型
     */
    @Column(name = "type")
    private String type;

    /**
     * 提交之前
     */
    @Column(name = "`before`")
    private String before;

    /**
     * 提交之后
     */
    @Column(name = "after")
    private String after;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 表名
     */
    @Column(name = "table_name")
    private String tableName;
}
