package com.lbl.microservice.utils;

import cn.hutool.json.JSONUtil;
import com.lbl.microservice.entity.TcLog;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.statement.update.Update;
import net.sf.jsqlparser.util.deparser.ExpressionDeParser;
import net.sf.jsqlparser.util.deparser.SelectDeParser;

import java.io.StringReader;
import java.util.List;
import java.util.Map;

public class SqlParserUtil{
	/**
	 * 将删除的sql转换成查询条件
	 * @param sql
	 * @param
	 * @return
	 */
	public static String sqlParserForDelete(String sql) {
		StringBuilder buffer = new StringBuilder();
		try {
			Delete tmt =(Delete)CCJSqlParserUtil.parse(sql);
			buffer.append("SELECT * FROM "+tmt.getTable().getName()+" WHERE "+tmt.getWhere().toString());
		} catch (JSQLParserException e) {
			e.printStackTrace();
		}
		return buffer.toString();
	}

	/**
	 * 将删除的sql转换成查询条件
	 * @param sql
	 * @param
	 * @return
	 */
	public static String sqlParserForUpdate(String sql) {
		StringBuilder buffer = new StringBuilder();
		try {
			Update tmt =(Update)CCJSqlParserUtil.parse(sql);
			buffer.append("SELECT * FROM "+tmt.getTable().getName()+" WHERE "+tmt.getWhere().toString());
		} catch (JSQLParserException e) {
			e.printStackTrace();
		}
		return buffer.toString();
	}
	public static String sqlParserForTableName(String sql) {
		String tableName="";
		try {
			Statement parse = CCJSqlParserUtil.parse(sql);
			if(parse instanceof Update){
				tableName=((Update)parse).getTable().getName();
			}else if(parse instanceof Delete){
				tableName=((Delete)parse).getTable().getName();
			}else if(parse instanceof Insert){
				tableName=((Insert)parse).getTable().getName();
			}
		} catch (JSQLParserException e) {
			e.printStackTrace();
		}
		return tableName;
	}
	public static void main(String[] args) {
		// String sqlParserForInsert =
		// sqlParserForInsert("Insert into Table2 select  *  from Table1",
		// "two");
		String json="[{'create_time':1579158603000,'money':100,'user_id':1,'id':33,'product_name':'电脑'}]";
		System.out.println(JSONUtil.toList(JSONUtil.parseArray(json), Map.class));
	}

}