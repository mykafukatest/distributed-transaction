package com.lbl.microservice.proxy;

import com.lbl.microservice.interceptor.SqlParsingInterceptor;
import com.lbl.microservice.mapper.TcLogMapper;
import com.lbl.microservice.utils.SpringUtil;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/18 9:28
 */
@Configuration
public class TransactionalProxyRegistrar implements ApplicationListener<ContextRefreshedEvent>,ApplicationContextAware {
    private ApplicationContext applicationContext;
    @Bean
    @ConditionalOnClass(SqlParsingInterceptor.class)
    public SqlParsingInterceptor sqlParsingInterceptor(){
        SqlParsingInterceptor sqlParsingInterceptor = new SqlParsingInterceptor();
        return sqlParsingInterceptor;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (event.getApplicationContext().getParent() == null) {
            SqlSessionFactory sessionFactory = applicationContext.getBean(SqlSessionFactory.class, "sqlSessionFactory");
            for (Interceptor interceptor : sessionFactory.getConfiguration().getInterceptors()) {
                System.out.println(interceptor.getClass());
            }
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }
}
