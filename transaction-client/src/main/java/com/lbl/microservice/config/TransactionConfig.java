package com.lbl.microservice.config;

import com.lbl.microservice.interceptor.RestTransactionInterceptor;
import com.lbl.microservice.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/18 16:18
 */
@Configuration
public class TransactionConfig {

    @Bean
    public IdWorker idWorker(){
        return new IdWorker();
    }
    @Bean
    public ThreadLocal<String> transactionGroupThreadLocal(){
        return new ThreadLocal<>();
    }
    @Bean
    public RestTransactionInterceptor restTransactionInterceptor(){
        return new RestTransactionInterceptor();
    }
    @Bean
    public RestTemplate restTemplate(@Autowired RestTransactionInterceptor transactionInterceptor){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(Collections.singletonList(transactionInterceptor));
        return restTemplate;
    }

}
