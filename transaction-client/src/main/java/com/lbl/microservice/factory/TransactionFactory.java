package com.lbl.microservice.factory;

import cn.hutool.json.JSONUtil;
import com.lbl.microservice.annotation.LblTransactional;
import com.lbl.microservice.aspectJ.TransactionUnit;
import com.lbl.microservice.aspectJ.TransferObject;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/18 9:32
 */
@Slf4j
public class TransactionFactory extends Thread implements InitializingBean {


    private volatile LinkedBlockingQueue<String> linkedBlockingQueue = new LinkedBlockingQueue<>();

    private volatile LinkedBlockingQueue<TransactionUnit> finalLinkedBlockingQueue = new LinkedBlockingQueue<>();


    private EventLoopGroup group = new NioEventLoopGroup();

    private Bootstrap bootstrap = new Bootstrap().group(group).channel(NioSocketChannel.class).handler(new ProduceInitializer(linkedBlockingQueue, finalLinkedBlockingQueue));

    @Value("${lbl.tm.netty.address}")
    private String address;

    @Value("${lbl.tm.netty.port}")
    private Integer port;

    @Value("${lbl.tm.netty.connect.time}")
    private long disconnectTime;

    private Channel channel;

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(disconnectTime);
                if(!channel.isActive()){
                    this.channel = bootstrap.connect(address, port).sync().channel();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendPreSubmissionTransactionalRequest(TransactionUnit transactionUnit) {
        log.info("2.transactionId={}发送预提交事务", transactionUnit.getTransactionId());
        TransferObject sendMsg = this.sendMsg(transactionUnit);
        log.info("3.transactionId={}预提tm请求结果", sendMsg.getTransactionUnit().getTransactionId());
    }

    public void sendSubmissionTransactionalRequest(TransactionUnit transactionUnit) {
        log.info("4.transactionId={}发送可以提交事务", transactionUnit.getTransactionId());
        this.sendMsg(transactionUnit);
        log.info("5.transactionId={}可以提交事务返回值", transactionUnit.getTransactionId());
    }

    public void sendRollbackTransactionalRequest(TransactionUnit transactionUnit) {
        log.info("5.transactionId={}发送回滚消息", transactionUnit.getTransactionId());
        this.sendMsg(transactionUnit);
        log.info("6.transactionId={}回滚消息tm请求结果", transactionUnit.getTransactionId());
    }

    public void sendFinalTransactionalRequest(TransactionUnit transactionUnit) {
        log.info("4.transactionId={}发送最终结果", transactionUnit.getTransactionId());
        this.sendMsg(transactionUnit);
        log.info("5.transactionId={}最终结果tm返回值", transactionUnit.getTransactionId());
    }

    public Connection startTransactionAndBuildConnection(DataSourceTransactionManager dataSourceTransactionManager, LblTransactional lblTransactional) {
        Connection connection = null;
        try {
            connection = dataSourceTransactionManager.getDataSource().getConnection();
            connection.setAutoCommit(false);
            connection.setReadOnly(lblTransactional.readOnly());
            if (lblTransactional.isolation().value() != -1) {
                connection.setTransactionIsolation(lblTransactional.isolation().value());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public TransactionDefinition startTransactionAndBuildTransactionDefinition(DataSourceTransactionManager dataSourceTransactionManager, LblTransactional lblTransactional) {
        DefaultTransactionDefinition definition = new DefaultTransactionDefinition();
        definition.setTimeout(lblTransactional.timeout());
        definition.setReadOnly(lblTransactional.readOnly());
        if (lblTransactional.isolation().value() != -1) {
            definition.setIsolationLevel(lblTransactional.isolation().value());
        }
        return definition;
    }

    private TransferObject sendMsg(TransactionUnit transactionUnit) {
        channel.writeAndFlush(JSONUtil.toJsonStr(TransferObject.rollbackTransaction(transactionUnit)));
        TransferObject transferObject = null;
        try {
            String take = linkedBlockingQueue.take();
            transferObject = JSONUtil.toBean(take, TransferObject.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return transferObject;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.channel = bootstrap.connect(address, port).sync().channel();
        this.start();
    }

    public static class ProduceInitializer extends ChannelInitializer<SocketChannel> {
        private volatile LinkedBlockingQueue<String> linkedBlockingQueue;
        private volatile LinkedBlockingQueue<TransactionUnit> finalLinkedBlockingQueue;

        public ProduceInitializer(LinkedBlockingQueue<String> linkedBlockingQueue, LinkedBlockingQueue<TransactionUnit> finalLinkedBlockingQueue) {
            this.linkedBlockingQueue = linkedBlockingQueue;
            this.finalLinkedBlockingQueue = finalLinkedBlockingQueue;
        }

        protected void initChannel(SocketChannel ch) throws Exception {
            ChannelPipeline pi = ch.pipeline();
            pi.addLast("decoder", new StringDecoder());
            pi.addLast("encoder", new StringEncoder());
            pi.addLast("handler", new ProduceHandler(linkedBlockingQueue, finalLinkedBlockingQueue));
        }
    }

    public static class ProduceHandler extends SimpleChannelInboundHandler<String> {

        private volatile LinkedBlockingQueue<String> linkedBlockingQueue;

        private volatile LinkedBlockingQueue<TransactionUnit> finalLinkedBlockingQueue;

        public ProduceHandler(LinkedBlockingQueue<String> linkedBlockingQueue, LinkedBlockingQueue<TransactionUnit> finalLinkedBlockingQueue) {
            this.linkedBlockingQueue = linkedBlockingQueue;
            this.finalLinkedBlockingQueue = finalLinkedBlockingQueue;
        }

        @Override
        public void channelActive(ChannelHandlerContext ctx) throws Exception {
            log.info("事务激活中...");
        }

        protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
            TransferObject transferObject = JSONUtil.toBean(msg, TransferObject.class);
            switch (transferObject.getCode()) {
                case 201:
                    linkedBlockingQueue.put(msg);
                    break;
                case 202:
                    finalLinkedBlockingQueue.put(transferObject.getTransactionUnit());
                    break;
                default:
            }

        }

    }

    public LinkedBlockingQueue<String> getLinkedBlockingQueue() {
        return linkedBlockingQueue;
    }

    public void setLinkedBlockingQueue(LinkedBlockingQueue<String> linkedBlockingQueue) {
        this.linkedBlockingQueue = linkedBlockingQueue;
    }

    public LinkedBlockingQueue<TransactionUnit> getFinalLinkedBlockingQueue() {
        return finalLinkedBlockingQueue;
    }

    public void setFinalLinkedBlockingQueue(LinkedBlockingQueue<TransactionUnit> finalLinkedBlockingQueue) {
        this.finalLinkedBlockingQueue = finalLinkedBlockingQueue;
    }
}
