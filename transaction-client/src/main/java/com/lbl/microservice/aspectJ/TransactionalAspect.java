package com.lbl.microservice.aspectJ;

import cn.hutool.json.JSONUtil;
import com.lbl.microservice.annotation.LblTransactional;
import com.lbl.microservice.annotation.TbhTransactional;
import com.lbl.microservice.entity.TcLog;
import com.lbl.microservice.factory.TransactionFactory;
import com.lbl.microservice.mapper.TcLogMapper;
import com.lbl.microservice.utils.IdWorker;
import com.lbl.microservice.utils.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/18 9:35
 */
@Aspect
@Slf4j
public class TransactionalAspect {

    private static final String LBL_TRANSACTION_GROUP_PREFIX = "GROUP_LBL";

    private static final String TBH_TRANSACTION_GROUP_PREFIX = "GROUP_TBH";

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TransactionFactory transactionFactory;

    @Autowired
    private ThreadLocal<String> transactionGroupThreadLocal;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Around("@annotation(lblTransactional)")
    public Object aroundLblAnnotation(ProceedingJoinPoint joinPoint, LblTransactional lblTransactional) throws Throwable {
        DataSourceTransactionManager dataSourceTransactionManager = ((DataSourceTransactionManager) transactionManager);
        TransactionDefinition definition = transactionFactory.startTransactionAndBuildTransactionDefinition(dataSourceTransactionManager, lblTransactional);
        TransactionStatus status = dataSourceTransactionManager.getTransaction(definition);
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        javax.servlet.http.HttpServletRequest request = attributes.getRequest();
        long transactionId = idWorker.nextId();
        String groupId = request.getHeader("TRANSACTION_GROUP");
        if (StringUtils.isEmpty(groupId)) {
            groupId = LBL_TRANSACTION_GROUP_PREFIX + transactionId;
        }
        log.info("1.transactionId={}开始事务",transactionId);
        transactionGroupThreadLocal.set(groupId);
        TransactionUnit transactionUnit = new TransactionUnit();
        transactionUnit.setTransactionId(String.valueOf(transactionId));
        transactionUnit.setTransactionGroup(groupId);
        transactionUnit.setType(1);
        transactionFactory.sendPreSubmissionTransactionalRequest(transactionUnit);
        Object proceed = null;
        try {
            proceed = joinPoint.proceed();
        } catch (Throwable throwable) {
            log.info("4.transactionId={}事务回滚:",transactionUnit.getTransactionId());
            transactionUnit.setType(3);
            transactionFactory.sendRollbackTransactionalRequest(transactionUnit);
            dataSourceTransactionManager.rollback(status);
            throw new RuntimeException(throwable);
        }
        if (StringUtils.isEmpty(request.getHeader("TRANSACTION_GROUP"))) {
            transactionUnit.setType(4);
            transactionFactory.sendFinalTransactionalRequest(transactionUnit);
        } else {
            transactionUnit.setType(2);
            transactionFactory.sendSubmissionTransactionalRequest(transactionUnit);
        }
        threadPoolTaskExecutor.execute(() -> {
            try {
                TransactionUnit unit = transactionFactory.getFinalLinkedBlockingQueue().take();
                String transactionGroup = unit.getTransactionGroup();
                TcLogMapper bean = SpringUtil.getBean(TcLogMapper.class);
                if (Objects.equals(unit.getType(), 3)) {
                    log.info("~~~~~~transactionId={}事务回滚~~~:",transactionGroup);
                    Example example=new Example(TcLog.class);
                    example.createCriteria().andEqualTo("groupId",transactionGroup);
                    TcLog logs = bean.selectOneByExample(example);
                    if(Objects.nonNull(logs)){
                        if(Objects.equals(logs.getType(),"INSERT")){
                            jdbcTemplate.update("DELETE FROM ? WHERE id=?",logs.getTableName(),logs.getAfter());
                        }else if(Objects.equals(logs.getType(),"DELETE")){
                            String before = logs.getBefore();
                            List<Map> maps = JSONUtil.toList(JSONUtil.parseArray(before), Map.class);
                            String sql[]=createSqlBatch(maps,logs.getTableName());
                            jdbcTemplate.batchUpdate(sql);
                        }else if(Objects.equals(logs.getType(),"UPDATE")){
                            String before = logs.getBefore();
                            List<Map> maps = JSONUtil.toList(JSONUtil.parseArray(before), Map.class);
                            List<String> ids = maps.stream().map(e -> e.get("id").toString()).collect(Collectors.toList());
                            String join = String.join(",", ids);
                            String delSql="DELETE FROM "+logs.getTableName()+" where id in ("+join+")";
                            jdbcTemplate.update(delSql);
                            String sql[]=createSqlBatch(maps,logs.getTableName());
                            jdbcTemplate.batchUpdate(sql);
                        }
                    }
//                    jdbcTemplate.update("DELETE FROM tc_log WHERE group_id=?",transactionGroup);
                } else if (Objects.equals(unit.getType(), 4)) {
                    log.info("~~~~~~transactionId={}事务提交~~~:",transactionGroup);
                    jdbcTemplate.update("DELETE FROM tc_log WHERE group_id=?",transactionGroup);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        dataSourceTransactionManager.commit(status);
        transactionGroupThreadLocal.remove();
        return proceed;
    }

    private static  String[] createSqlBatch(List<Map> maps,String tableName) {
        String sql[]=new String[maps.size()];
        for (int i = 0; i < maps.size(); i++) {
            Map map= maps.get(i);
            Set<Map.Entry> set = map.entrySet();
            Iterator<Map.Entry> iterator = set.iterator();
            StringBuffer c_buffer=new StringBuffer();
            StringBuffer v_buffer=new StringBuffer();
            int j=0;
            while (iterator.hasNext()){
                Map.Entry next = iterator.next();
                c_buffer.append(next.getKey());
                v_buffer.append("'"+next.getValue()+"'");
                if(j!=set.size()-1){
                    c_buffer.append(",");
                    v_buffer.append(",");
                }
                j++;
            }
            sql[i]="INSERT INTO "+tableName+"("+c_buffer.toString()+") VALUES("+v_buffer.toString()+");";
        }
        return sql;
    }

    public static void main(String[] args) {
        String json="[{'create_time':1579158603000,'money':100,'user_id':1,'id':33,'product_name':'电脑'},{'create_time':1579158603000,'money':100,'user_id':1,'id':34,'product_name':'电脑'}]";
        List<Map> maps = JSONUtil.toList(JSONUtil.parseArray(json), Map.class);
        System.out.println(Arrays.toString(createSqlBatch(maps, "tb_order")));


    }
    @Around("@within(lblTransactional)")
    public Object aroundLblWithin(ProceedingJoinPoint joinPoint, LblTransactional lblTransactional) throws Throwable {
        return joinPoint.proceed();
    }

    @Around("@annotation(tbhTransactional)")
    public Object aroundTbhAnnotation(ProceedingJoinPoint joinPoint, TbhTransactional tbhTransactional) throws Throwable {
        return joinPoint.proceed();
    }

    @Around("@within(tbhTransactional)")
    public Object aroundTbhWithin(ProceedingJoinPoint joinPoint, TbhTransactional tbhTransactional) throws Throwable {
        return joinPoint.proceed();
    }
}
