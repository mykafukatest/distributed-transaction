package com.lbl.microservice.utils;

/**
 * @author : FallenRunning (汤北寒)
 * @date : 2019/10/16/17:26
 */
public interface ResponseCode {
    Integer SUCCESS = 200;
    Integer ERROR = 500;
    Integer INVALID = 403;
}
