package com.lbl.microservice.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author : FallenRunning (汤北寒)
 * @date : 2019/10/16/17:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResponseResult {
    private Integer code;
    private String msg;
    private Object data;
    private boolean success;

    public ResponseResult(Integer code , String msg , boolean success){
        this.code = code;
        this.msg = msg;
        this.success = success;
    }
    public static ResponseResult success(Object data){
        return new ResponseResult(ResponseCode.SUCCESS,"OK",data,true);
    }

    public static ResponseResult success(){
        return new ResponseResult(ResponseCode.SUCCESS,"OK",true);
    }

    public static ResponseResult error(String msg){
        return new ResponseResult(ResponseCode.ERROR,msg,null,false);
    }
    public static ResponseResult error(){
        return new ResponseResult(ResponseCode.ERROR,"NO",false);
    }
}
