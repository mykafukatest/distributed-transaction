package com.lbl.microservice.aspectJ;

import com.lbl.microservice.enums.TransferStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/17 16:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferObject implements Serializable {

    private Integer code;

    private String msg;

    private TransactionUnit transactionUnit;

    public static TransferObject success(String msg, TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE200.getCode(), msg, transactionUnit);
    }

    public static TransferObject error(String msg) {
        return new TransferObject(TransferStatus.CODE500.getCode(), msg, null);
    }

    public static TransferObject noPermission(String msg) {
        return new TransferObject(TransferStatus.CODE401.getCode(), msg, null);
    }

    public static TransferObject preCommitTransaction(TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE200.getCode(), "预提交事务", transactionUnit);
    }

    public static TransferObject preCommitTransactionResult(TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE201.getCode(), "预提交事务请求结果", transactionUnit);
    }

    public static TransferObject commitTransaction(TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE200.getCode(), "提交事务", transactionUnit);
    }

    public static TransferObject commitTransactionResult(TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE201.getCode(), "提交事务请求结果", transactionUnit);
    }

    public static TransferObject commitFinalTransactionResult(TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE202.getCode(), "提交事务最终结果", transactionUnit);
    }

    public static TransferObject rollbackTransaction(TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE200.getCode(), "回滚事务", transactionUnit);
    }

    public static TransferObject rollbackTransactionResult(TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE201.getCode(), "回滚事务请求结果", transactionUnit);
    }

    public static TransferObject rollbackFinalTransactionResult(TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE202.getCode(), "回滚事务最终结果", transactionUnit);
    }

    public static TransferObject finalTransactionResult(TransactionUnit transactionUnit) {
        return new TransferObject(TransferStatus.CODE201.getCode(), "二段式请求结果", transactionUnit);
    }
}
