package com.lbl.microservice.aspectJ;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/17 16:25
 */
@Data
public class TransactionUnit implements Serializable {
    /**
     * 事务id唯一表示
     */
    private String transactionId;
    /**
     * 事务组
     */
    private String transactionGroup;
    /**
     * 事务发送消息的类型 1.预提交 2.提交事务 3.回滚  4真正提交事务
     */
    private Integer type;
}
