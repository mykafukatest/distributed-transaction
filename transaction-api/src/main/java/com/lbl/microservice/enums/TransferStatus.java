package com.lbl.microservice.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/17 17:31
 */
@Getter
@AllArgsConstructor
public enum TransferStatus {
   CODE200(200, "成功"),
   CODE201(201, "结果"),
   CODE202(202, "最终"),
   CODE500(500, "失败"),
   CODE401(401, "无权限");
   /**
    * 状态
    */
   private Integer code;
   /**
    * 描述
    */
   private String name;

   public static Integer getValue(TransferStatus transferStatus){
      return transferStatus.getCode();
   }
}
