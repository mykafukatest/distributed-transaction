package com.lbl.microservice.transactionmanager.listener;

import com.lbl.microservice.transactionmanager.netty.ServerStart;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;


/**
 * @author lbl
 */
@Component
@Slf4j
public class StartNettyListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Value("${lbl.tm.netty.port}")
    private int port;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (event.getApplicationContext().getParent() == null) {
            ServerStart serverStart = ServerStart.serverInstance(port);
            threadPoolTaskExecutor.execute(serverStart);
            log.info("ServerStart .......... port ={}", port);
        }
    }
}
