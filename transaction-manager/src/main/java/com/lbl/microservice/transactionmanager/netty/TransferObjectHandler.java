package com.lbl.microservice.transactionmanager.netty;

import cn.hutool.json.JSONUtil;
import com.lbl.microservice.aspectJ.TransferObject;
import com.lbl.microservice.enums.TransferStatus;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * @author lbl
 */
@Slf4j

public class TransferObjectHandler extends SimpleChannelInboundHandler<String> {
    private static ChannelGroup clients = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        if(!StringUtils.isEmpty(msg)){
            TransferObject bean = JSONUtil.toBean(msg, TransferObject.class);
            if(Objects.equals(bean.getCode(), TransferStatus.getValue(TransferStatus.CODE200))){
                TransactionUnitPersistence.associatedTransactionsAndChannels(bean.getTransactionUnit(),ctx.channel().id().asLongText());
                switch (bean.getTransactionUnit().getType()){
                    case 1:
                        //预提交
                        doPreCommitTransaction(bean);
                        ctx.channel().writeAndFlush(JSONUtil.toJsonStr(TransferObject.preCommitTransactionResult(bean.getTransactionUnit())));
                        break;
                    case 2:
                        //提交
                        doCommitTransaction(bean);
                        ctx.channel().writeAndFlush(JSONUtil.toJsonStr(TransferObject.commitTransactionResult(bean.getTransactionUnit())));
                        break;
                    case 3:
                        //回滚
                        doRollbackTransaction(bean);
                        ctx.channel().writeAndFlush(JSONUtil.toJsonStr(TransferObject.rollbackTransactionResult(bean.getTransactionUnit())));
                        break;
                    case 4:
                        //真正提交
                        doOverCommitTransaction(bean);
                        ctx.channel().writeAndFlush(JSONUtil.toJsonStr(TransferObject.finalTransactionResult(bean.getTransactionUnit())));
                        break;
                    default:
                        //默认处理
                        doNoneExecution(bean);
                }
            }

        }
    }

    private void doOverCommitTransaction(TransferObject bean) {
        TransactionUnitPersistence.doOverCommitTransaction(bean,clients);
    }

    private void doNoneExecution(TransferObject bean) {
    }

    private void doRollbackTransaction(TransferObject bean) {
        TransactionUnitPersistence.rollbackTransaction(bean,clients);
    }

    private void doCommitTransaction(TransferObject bean) {
        TransactionUnitPersistence.commitTransaction(bean,clients);
    }

    private void doPreCommitTransaction(TransferObject bean) {
        TransactionUnitPersistence.preCommitTransaction(bean,JSONUtil.toJsonStr(bean.getTransactionUnit()));
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        log.info("建立连接client ip:{}", ctx.channel().remoteAddress());
        clients.add(ctx.channel());
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        log.info("断开长ID为:{}", ctx.channel().id().asLongText());
        log.info("断开短ID为:{}", ctx.channel().id().asShortText());
        //删除channel
        clients.remove(ctx.channel());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        Channel channel = ctx.channel();
        channel.close();
        clients.remove(channel);
    }

}
