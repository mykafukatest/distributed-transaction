package com.lbl.microservice.transactionmanager.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lbl
 */
@RestController
@RequestMapping("/index")
public class IndexController {

    @RequestMapping("/hello")
    public String execute(){
        return "hello";
    }
}
