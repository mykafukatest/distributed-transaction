package com.lbl.microservice.transactionmanager.netty;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.json.JSONUtil;
import com.lbl.microservice.aspectJ.TransactionUnit;
import com.lbl.microservice.aspectJ.TransferObject;
import com.lbl.microservice.transactionmanager.utils.SpringUtil;
import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author lbl
 * @Description: 用户id和channel的关联关系处理
 */
@Slf4j
public class TransactionUnitPersistence {

    private static final String TIMEOUT = "lbl.tm.netty.transaction.channel.timeout";

    public static void associatedTransactionsAndChannels(TransactionUnit transactionUnit, String channelId) {
        log.info("事务id={}、事务组group={} 建立通道关联", transactionUnit.getTransactionId(),
                transactionUnit.getTransactionGroup());

        RedisTemplate<String, String> template = SpringUtil.getBean(StringRedisTemplate.class);

        template.opsForValue().set(transactionUnit.getTransactionId(), channelId, Integer.parseInt(SpringUtil.getProperties(TIMEOUT)), TimeUnit.SECONDS);
    }


    public static void preCommitTransaction(TransferObject transferObject, String transactionUnit) {
        log.info("事务id={}、事务组group={} 状态 type={}", transferObject.getTransactionUnit().getTransactionId(),
                transferObject.getTransactionUnit().getTransactionGroup(),
                transferObject.getTransactionUnit().getType());

        RedisTemplate<String, String> template = SpringUtil.getBean(StringRedisTemplate.class);

        template.opsForList().leftPush(transferObject.getTransactionUnit().getTransactionGroup(), transactionUnit);
    }

    public static void rollbackTransaction(TransferObject transferObject, ChannelGroup channelGroup) {
        log.info("事务id={}、事务组group={}建立关联 状态 type={}", transferObject.getTransactionUnit().getTransactionId(),
                transferObject.getTransactionUnit().getTransactionGroup(),
                transferObject.getTransactionUnit().getType());
        findChannelAndSendTransferMsg(transferObject, channelGroup);
    }

    public static void commitTransaction(TransferObject transferObject, ChannelGroup channelGroup) {
        log.info("事务id={}、事务组group={}建立关联 状态 type={}", transferObject.getTransactionUnit().getTransactionId(),
                transferObject.getTransactionUnit().getTransactionGroup(),
                transferObject.getTransactionUnit().getType());
        findChannelAndSendTransferMsg(transferObject, channelGroup);
    }

    private static Channel findChannelById(String channelId, ChannelGroup channelGroup) {
        Channel channelResult = null;
        for (Channel channel : channelGroup) {
            if (Objects.equals(channel.id().asLongText(), channelId)) {
                channelResult = channel;
            }
        }
        return channelResult;
    }

    private static void findChannelAndSendTransferMsg(TransferObject transferObject, ChannelGroup channelGroup) {
        RedisTemplate<String, String> template = SpringUtil.getBean(StringRedisTemplate.class);
        Long size = template.opsForList().size(transferObject.getTransactionUnit().getTransactionGroup());
        List<TransactionUnit> unitList=new ArrayList<>();
        for (int i = 0; i < size; i++) {
            String pop = template.opsForList().leftPop(transferObject.getTransactionUnit().getTransactionGroup());
            TransactionUnit unit = JSONUtil.toBean(pop, TransactionUnit.class);
            if(Objects.equals( unit.getTransactionId(),transferObject.getTransactionUnit().getTransactionId())){
                unit.setType(transferObject.getTransactionUnit().getType());
            }
            unitList.add(unit);
        }
        if (CollectionUtil.isNotEmpty(unitList)) {
            template.opsForList().leftPushAll(transferObject.getTransactionUnit().getTransactionGroup(), unitList.stream().map(JSONUtil::toJsonStr).collect(Collectors.toList()));
        }
    }

    public static void doOverCommitTransaction(TransferObject bean, ChannelGroup clients) {
        RedisTemplate<String, String> template = SpringUtil.getBean(StringRedisTemplate.class);
        List<String> range = template.opsForList().range(bean.getTransactionUnit().getTransactionGroup(), 0, -1);
        if (CollectionUtil.isNotEmpty(range)) {
            List<TransactionUnit> unitList = range.stream().map(e -> JSONUtil.toBean(e, TransactionUnit.class)).filter(e->Objects.equals(e.getType(), 3)).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(unitList)) {
                List<TransactionUnit> list = range.stream().map(e -> JSONUtil.toBean(e, TransactionUnit.class)).collect(Collectors.toList());
                list.forEach(e -> {
                    String channelId = template.opsForValue().get(e.getTransactionId());
                    Channel channel = findChannelById(channelId, clients);
                    if (channel != null) {
                        TransactionUnit unit=new TransactionUnit();
                        unit.setTransactionId(e.getTransactionId());
                        unit.setTransactionGroup(bean.getTransactionUnit().getTransactionGroup());
                        unit.setType(3);
                        channel.writeAndFlush(JSONUtil.toJsonStr(TransferObject.rollbackFinalTransactionResult(unit)));
                    }
                });
            }else{
                List<TransactionUnit> list = range.stream().map(e -> JSONUtil.toBean(e, TransactionUnit.class)).collect(Collectors.toList());
                list.forEach(e -> {
                    String channelId = template.opsForValue().get(e.getTransactionId());
                    Channel channel = findChannelById(channelId, clients);
                    if (channel != null) {
                        TransactionUnit unit=new TransactionUnit();
                        unit.setTransactionId(e.getTransactionId());
                        unit.setTransactionGroup(bean.getTransactionUnit().getTransactionGroup());
                        unit.setType(4);
                        channel.writeAndFlush(JSONUtil.toJsonStr(TransferObject.commitFinalTransactionResult(unit)));
                    }
                });
            }
        }
    }
}
