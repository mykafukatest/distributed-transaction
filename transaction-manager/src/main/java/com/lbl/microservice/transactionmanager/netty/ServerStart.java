package com.lbl.microservice.transactionmanager.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.Data;

/**
 * @author lbl
 */
@Data
public class ServerStart implements Runnable {
    private Integer port;

    private static ServerStart SERVERSTART = new ServerStart();
    private EventLoopGroup bossGroup;
    private EventLoopGroup subGroup;
    private ServerBootstrap bootstrap;

    private ServerStart() {
    }

    public static ServerStart serverInstance(Integer port) {
        SERVERSTART.setPort(port);
        return SERVERSTART;
    }

    private void start() {
        bossGroup = new NioEventLoopGroup();
        subGroup = new NioEventLoopGroup();
        bootstrap = new ServerBootstrap();
        try {
            bootstrap.group(bossGroup, subGroup).
                    channel(NioServerSocketChannel.class).
                    childHandler(new TransactionServerInitialzer());
            ChannelFuture sync = bootstrap.bind(port).sync();
            sync.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            subGroup.shutdownGracefully();
        }
    }

    @Override
    public void run() {
        start();
    }
}
