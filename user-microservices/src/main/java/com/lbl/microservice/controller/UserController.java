package com.lbl.microservice.controller;

import com.lbl.microservice.service.TbUserService;
import com.lbl.microservice.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lbl
 * @version 1.0
 * @date 2019/12/18 14:59
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private TbUserService tbUserService;

    @RequestMapping("/{userId}")
    public ResponseResult userOrders(@PathVariable("userId")Integer userId){
        tbUserService.placeAnOrder(userId);
        return ResponseResult.success();
    }
}
