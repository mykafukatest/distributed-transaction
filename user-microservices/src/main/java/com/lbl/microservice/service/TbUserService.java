package com.lbl.microservice.service;


import com.lbl.microservice.entity.TbUser;

/**
 * @author 刘祖明
 * @date 2019/12/18 14:23
 */
public interface TbUserService {

    int deleteByPrimaryKey(Integer id);

    int insert(TbUser record);

    int insertSelective(TbUser record);

    TbUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbUser record);

    int updateByPrimaryKey(TbUser record);

    void placeAnOrder(Integer userId);
}


