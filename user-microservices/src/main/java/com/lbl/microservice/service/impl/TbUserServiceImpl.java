package com.lbl.microservice.service.impl;

import com.lbl.microservice.annotation.LblTransactional;
import com.lbl.microservice.entity.TbOrder;
import com.lbl.microservice.entity.TbUser;
import com.lbl.microservice.mapper.TbUserMapper;
import com.lbl.microservice.service.TbUserService;
import com.lbl.microservice.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Random;

/**
 * @author 刘祖明
 * @date 2019/12/18 14:23
 */
@Service
public class TbUserServiceImpl implements TbUserService {
    @Autowired
    private RestTemplate restTemplate;

    @Resource
    private TbUserMapper tbUserMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return tbUserMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(TbUser record) {
        return tbUserMapper.insert(record);
    }

    @Override
    public int insertSelective(TbUser record) {
        return tbUserMapper.insertSelective(record);
    }

    @Override
    public TbUser selectByPrimaryKey(Integer id) {
        return tbUserMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(TbUser record) {
        return tbUserMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(TbUser record) {
        return tbUserMapper.updateByPrimaryKey(record);
    }

    @Override
    @LblTransactional
    public void placeAnOrder(Integer userId) {
        TbUser tbUser = tbUserMapper.selectByPrimaryKey(userId);
        Float money = tbUser.getMoney();
        Random random = new Random();
        if(money>0){
            int deductMoney = random.nextInt(100);
            int newDeductMoney= deductMoney==0?money.intValue() :deductMoney;
            TbUser user=new TbUser();
            user.setId(tbUser.getId());
            user.setMoney(money-newDeductMoney);
            tbUserMapper.updateByPrimaryKeySelective(user);

            TbOrder order = new TbOrder();
            order.setCreateTime(new Date());
            order.setMoney((float) newDeductMoney);
            order.setUserId(userId);
            order.setProductName("电脑");
            ResponseResult result = restTemplate.postForObject("http://127.0.0.1:8000/order/placeAnOrder", order, ResponseResult.class);
        }else{
            throw new RuntimeException("用户余额不足!");
        }
    }
}


