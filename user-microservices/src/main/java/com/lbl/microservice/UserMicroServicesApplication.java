package com.lbl.microservice;

import com.lbl.microservice.annotation.EnableTransactionClient;
import com.lbl.microservice.entity.TbUser;
import org.apache.catalina.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

/**
 *
 */
@SpringBootApplication
@MapperScan("com.lbl.microservice.mapper")
@EnableTransactionClient
public class UserMicroServicesApplication{

    public static void main(String[] args) {
        SpringApplication.run(UserMicroServicesApplication.class, args);
    }

}
