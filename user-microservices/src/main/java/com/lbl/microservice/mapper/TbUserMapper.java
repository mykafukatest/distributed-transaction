package com.lbl.microservice.mapper;

import com.lbl.microservice.entity.TbUser;
import tk.mybatis.mapper.common.Mapper;

public interface TbUserMapper extends Mapper<TbUser> {
}