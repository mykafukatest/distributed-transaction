package com.lbl.microservice.microservices;

import com.lbl.microservice.mapper.TbUserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserMicroservicesApplicationTests {

    @Autowired
    private TbUserMapper tbUserMapper;

    @Test
    void contextLoads() {
        System.out.println(tbUserMapper.selectAll());
    }

}
